# ht-base

Go base images, rebuilt every week

## ht-base/build

Build/dev env, included tools:

* alpine-sdk
* delve
* go
* golangci-lint
* make
* protoc
* swaggo
* tzdata
